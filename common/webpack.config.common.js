var path = require("path");
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  entry: {
    common: [
      "./main.js",
      "./utilityFunc1.js",
      "./utilityFunc2.js"
    ]
  },
  output: {
    path: path.resolve(__dirname, "../dist"),
    filename: "[name].dll.js",
    library: "[name]",
  },
  plugins: [
    new webpack.DllPlugin({
      path: path.join(__dirname, "../dist/[name]-manifest.json"),
      name: "[name]"
    })
  ],
};
