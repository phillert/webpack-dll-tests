import utilityFunc1Promise from 'common/main';
import { plugin1 } from 'plugin/plugin1';

/**
 * Calls func1 from common/utilityFunc1 via promise of main.js
 * The expectation matches the real: Webpack loads common/utilityFunc1 only one time.
 */
setTimeout(_ => {
  utilityFunc1Promise().then(utilityFunc1 => console.log('app:', utilityFunc1.func1()));
}, 2000);
