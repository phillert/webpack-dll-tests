const path = require("path");
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  context: __dirname,
  entry: "./app",
  output: {
    filename: "app.bundle.js",
    path: path.resolve(__dirname, "../dist")
  },
  plugins: [
    new webpack.DllReferencePlugin({
      scope: "common",
      manifest: require("../dist/common-manifest.json")
    }),
    new webpack.DllReferencePlugin({
      scope: "plugin",
      manifest: require("../dist/plugin1-manifest.json")
    })
  ],
};
