var path = require("path");
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  entry: {
    plugin1: ["./plugin1.js"]
  },
  output: {
    path: path.resolve(__dirname, "../dist"),
    filename: "[name].dll.js",
    library: "[name]",
  },
  plugins: [
    new webpack.DllReferencePlugin({
      scope: "common",
      manifest: require("../dist/common-manifest.json")
    }),
    new webpack.DllPlugin({
      path: path.join(__dirname, "../dist/[name]-manifest.json"),
      name: "[name]"
    }),
  ],
};
