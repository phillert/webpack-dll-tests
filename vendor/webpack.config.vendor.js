var path = require("path");
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  entry: {
    "angular-1.5.0": ["./angular/1.5.0/node_modules/angular"],
    "angular-1.6.8": ["./angular/1.6.8/node_modules/angular"]
  },
  output: {
    path: path.resolve(__dirname, "../dist"),
    filename: "[name].dll.js",
    library: "[name]_[chunkhash]",
  },
  plugins: [
    new webpack.DllPlugin({
      path: path.join(__dirname, "../dist/[name]-manifest.json"),
      name: "[name]"
    })
  ],
};
